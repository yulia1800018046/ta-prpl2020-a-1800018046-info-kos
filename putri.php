<?php 
    include 'knk.php';

      ?>
  <!DOCTYPE html>
  <html lang="en">
<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="assets/img/b.png">
    <link rel="icon" type="image/png" href="assets/img/b.png">    
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>Spesialis Kos</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
    
    <link href="bootstrap3/css/bootstrap.css" rel="stylesheet" />
    <link href="bootstrap3/css/font-awesome.css" rel="stylesheet" />
    
    <link href="assets/css/gsdk.css" rel="stylesheet" />   
    <link href="assets/css/demo.css" rel="stylesheet" /> 

    <!--     Font Awesome     -->
    <link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Grand+Hotel' rel='stylesheet' type='text/css'>
  
</head>

<body>
 <div id="navbar-full">
    <div id="navbar">
    <!--    
        navbar-default can be changed with navbar-ct-blue navbar-ct-azzure navbar-ct-red navbar-ct-green navbar-ct-orange  
        -->
        <nav class="navbar navbar-ct-blue navbar-transparent navbar-fixed-top" role="navigation">
          <div class="alert alert-success hidden">
            <div class="container">
                <b>Lorem ipsum</b> dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.
            </div>
          </div>
          
          <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="index1.php">SKOS</a>
            </div>
        
          <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
              <ul class="nav navbar-nav">
                <li class="active"><a href=https://instagram.com/yuliachairisa>Social Media</a></li>
                <li class="dropdown">
                      <a href="#gsdk" class="dropdown-toggle" data-toggle="dropdown">Tipe Kos<b class="caret"></b></a>
                      <ul class="dropdown-menu">
                        <li><a href="putri.php">Putri</a></li>
                        <li><a href="putra.php">Putra</a></li>
                        <li><a href="putricampur.php">Putri Campur</a></li>
                        <li><a href="putracampur.php">Putra Campur</a></li>
                      </ul>
                </li>
                <li>
                    <a href="javascript:void(0);" data-toggle="search" class="hidden-xs"><i class="fa fa-search"></i></a>
                </li>
              </ul>
               <form class="navbar-form navbar-left navbar-search-form" role="search">                  
                 <div class="form-group">
                      <input type="text" value="" class="form-control" placeholder="Search...">
                 </div> 
              </form>
              <ul class="nav navbar-nav navbar-right">
                    <li><a href="tambahpengunjung.php">Sign in</a></li>
               </ul>
              
            </div><!-- /.navbar-collapse -->
          </div><!-- /.container-fluid -->
        </nav>
        <div class="blurred-container">
        <div class="motto"><br>
            <div class="border">SPESIALIS</div>
            <div >KOS</div>
        </div>
            <div class="img-src" style="background-image: url('assets/img/a.jpg')"></div>
             <div class='img-src blur' style="background-image: url('assets/img/a.jpg')"></div>
        </div>
    </div><!--  end navbar -->

</div> <!-- end menu-dropdown -->
    
</div>     
    
<div class="main">
    <div class="container tim-container">
       
        <div class="tim-title">
            <h1>Kos Putri</h1>
        </div>

<div class="main">
    <div class="container tim-container">
        <div id="extras">
                <div class="col-md-7 col-md-offset-0 col-sm-10 col-sm-offset-1">
                    <div class="text-center">
                        <img src="assets/img/5.jpeg" alt="Rounded Image" class="img-rounded img-responsive img-dog">
                    </div>
                </div>
                <div class="col-md-5 col-sm-12">
                                <h2 class="text-center">Kos Putri Bagus
                                    </h2>
                                <hr>
                                <p>
                               Yogyakarta,Umbulharjo,No.42
                               Fasilitas:UK 4x5 ,Kamar mandi dalam,AC,Free WIFI dan Air
                               Isian & Kosongan,Bebas 24 Jam.
                               Rp.900.000,- sampai Rp.1.500.000,-
                                </p>
<div class="tim-title">
             <a href="tambahpesanan4.php"> <button class="add" class="btn btn-primary">Pesan Sekarang</button></a>
<br>
</div></div></div>
</div></div></div>
<div class="main">
    <div class="container tim-container">
        <div id="extras">
                <div class="col-md-7 col-md-offset-0 col-sm-10 col-sm-offset-1">
                    <div class="text-center">
                        <img src="assets/img/6.jpeg" alt="Rounded Image" class="img-rounded img-responsive img-dog">
                    </div>
                </div>
                <div class="col-md-5 col-sm-12">
                                <h2 class="text-center">Kos Putri Yulia
                                    </h2>
                                <hr>
                                <p>
                                Yogyakarta,Pogung Kidul,Jl.Pandeyan NO.10
                                Fasilitas:UK 4x4 ,Kamar mandi dalam,AC,
                                Kosongan,Bebas 24 Jam.
                                Rp.1.000.000,- 
                                </p>
<div class="tim-title">
<a href="tambahpesanan5.php"> <button class="add" class="btn btn-primary">Pesan Sekarang</button></a>
<br>
</div></div></div>
<div class="main">
    <div class="container tim-container">
        <div id="extras">
                <div class="col-md-7 col-md-offset-0 col-sm-10 col-sm-offset-1">
                    <div class="text-center">
                        <img src="assets/img/7.jpeg" alt="Rounded Image" class="img-rounded img-responsive img-dog">
                    </div>
                </div>
                <div class="col-md-5 col-sm-12">
                                <h2 class="text-center">Kos Putri Diana
                                    </h2>
                                <hr>
                                <p>
                                Yogyakarta,Palagan,Jl.Kalibata NO:15B
                                Fasilitas:UK 4x5 ,Kamar mandi dalam,Isian,
                                AC,Bebas 24 Jam.
                                Rp.1.500.000,- 
                                </p>
<div class="tim-title">
             <a href="tambahpesanan6.php"> <button class="add" class="btn btn-primary">Pesan Sekarang</button></a>
<br>
</div></div></div>
<div class="main">
    <div class="container tim-container">
        <div id="extras">
                <div class="col-md-7 col-md-offset-0 col-sm-10 col-sm-offset-1">
                    <div class="text-center">
                        <img src="assets/img/8.jpeg" alt="Rounded Image" class="img-rounded img-responsive img-dog">
                    </div>
                </div>
                <div class="col-md-5 col-sm-12">
                                <h2 class="text-center">Kos Putri Alena
                                    </h2>
                                <hr>
                                <p>
                                Yogyakarta,Gedong Kuning,Jl.Mergangsan, NO.109
                                Fasilitas:UK 4x4 ,Kamar mandi dalam,
                                Isian,AC,Bebas 24 Jam.
                                Rp.1.200.000,- 
                                </p>
                                <div class="tim-title">
             <a href="tambahpesanan7.php"> <button class="add" class="btn btn-primary">Pesan Sekarang</button></a>
<br>
</div></div></div>
</body>

    <script src="jquery/jquery-1.10.2.js" type="text/javascript"></script>
    <script src="assets/js/jquery-ui-1.10.4.custom.min.js" type="text/javascript"></script>

    <script src="bootstrap3/js/bootstrap.js" type="text/javascript"></script>
    <script src="assets/js/gsdk-checkbox.js"></script>
    <script src="assets/js/gsdk-radio.js"></script>
    <script src="assets/js/gsdk-bootstrapswitch.js"></script>
    <script src="assets/js/get-shit-done.js"></script>
    
    <script src="assets/js/custom.js"></script>

</html>
</script>
</html>